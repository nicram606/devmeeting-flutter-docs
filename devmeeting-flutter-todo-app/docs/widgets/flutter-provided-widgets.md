---
id: flutter-provided-widgets
title: Flutter-provided widgets
---

## Common widgets
Flutter comes with a suite of powerful basic widgets, of which the following are commonly used:

### [Text](https://api.flutter.dev/flutter/widgets/Text-class.html)
The Text widget lets you create a run of styled text within your application.

### [Row](https://api.flutter.dev/flutter/widgets/Row-class.html), [Column](https://api.flutter.dev/flutter/widgets/Column-class.html)
These flex widgets let you create flexible layouts in both the horizontal (Row) and vertical (Column) directions. The design of these objects is based on the web’s flexbox layout model.

### [Stack](https://api.flutter.dev/flutter/widgets/Stack-class.html)
Instead of being linearly oriented (either horizontally or vertically), a Stack widget lets you place widgets on top of each other in paint order. You can then use the Positioned widget on children of a Stack to position them relative to the top, right, bottom, or left edge of the stack. Stacks are based on the web’s absolute positioning layout model.

### [Container](https://api.flutter.dev/flutter/widgets/Container-class.html)
The Container widget lets you create a rectangular visual element. A container can be decorated with a BoxDecoration, such as a background, a border, or a shadow. A Container can also have margins, padding, and constraints applied to its size. In addition, a Container can be transformed in three dimensional space using a matrix. 

### [ListView](https://api.flutter.dev/flutter/widgets/ListView-class.html)
ListView is the most commonly used scrolling widget. It displays its children one after another in the scroll direction.

## Material widgets
In today's workshop we will be using Material based widgets in addition to common ones. In particular:

### [ListTile](https://api.flutter.dev/flutter/material/ListTile-class.html)
A list tile contains one to three lines of text optionally flanked by icons or other widgets, such as check boxes.

### [IconButton](https://api.flutter.dev/flutter/material/IconButton-class.html)
An icon button is a picture printed on a Material widget that reacts to touches by filling with color (ink). Icon buttons are commonly used in the AppBar.actions field, but they can be used in many other places as well.

### [TextField](https://api.flutter.dev/flutter/material/TextField-class.html)
A text field lets the user enter text, either with hardware keyboard or with an onscreen keyboard.

### [Scaffold](https://api.flutter.dev/flutter/material/Scaffold-class.html)
Implements the basic material design visual layout structure.

This class provides APIs for showing drawers, snack bars, and bottom sheets.

## Widgets catalog
To discover more widgets from Flutter, check the [Widgets catalog](https://flutter.dev/docs/development/ui/widgets).
