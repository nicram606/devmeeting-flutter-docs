---
id: task
title: Task
---

- Add your `TodoItem` class to your Flutter project
- Create a widget class called `Todo` accepting `TodoItem` instance as an argument. Use [ListTile](https://api.flutter.dev/flutter/material/ListTile-class.html) widget as a base.
- Replace `MyHomePage` contents with yours, that includes a [ListView](https://api.flutter.dev/flutter/widgets/ListView-class.html) and a few `Todo` widgets as children to make the list scroll. Make created listview a `body:` of a [Scaffold](https://api.flutter.dev/flutter/material/Scaffold-class.html).

## Expected result
![scrolling list of tasks to do](../../static/vid/flutter_basic_list.gif)
