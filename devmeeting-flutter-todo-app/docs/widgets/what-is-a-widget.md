---
id: what-is-a-widget
title: What is a widget
---

## Widgets are everything!
The basic building block of every Flutter app is a _Widget_. 

The central idea is that you build your UI out of widgets. Widgets describe what their view should look like given their current configuration and state. When a widget’s state changes, the widget rebuilds its description, which the framework diffs against the previous description in order to determine the minimal changes needed in the underlying render tree to transition from one state to the next.

## Widget types
There are Stateless and Stateful widgets. As they names suggest, the first type does not hold any state and the second one does. 

Stateless widgets are the pure representation of the idea of widget -- `build()` method of every widget must return `Widget` instance at the end of execution. If your widget needs to hold some information longer than one `build()` execution, you should use StatefulWidget -- more on that later.

When writing an app, you’ll commonly author new widgets that are subclasses of either StatelessWidget or StatefulWidget, depending on whether your widget manages any state. A widget’s main job is to implement a build() function, which describes the widget in terms of other, lower-level widgets. The framework builds those widgets in turn until the process bottoms out in widgets that represent the underlying RenderObject, which computes and describes the geometry of the widget.

### Sizing issue
If your are coming from an HTML/CSS layout world, you may be surprised at first of how Flutter manages sizing and positioning of widgets. There's a rule that will help you with that:
:::tip
## Constraints go down. Sizes go up. Positions are set by parents.
:::
