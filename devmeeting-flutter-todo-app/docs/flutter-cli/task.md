---
id: task
title: Task
---

- Change the app bar title to `Flutter Workshop` using hot-reload
- Change the accent color of the app to yellow using hot-reload
- Change the message above counter to 'Pairs counted:' using hot-reload
- Divide the counter value by 2 before displaying and `floor()` it using hot-reload

## Expected result
![expected result app screenshot](../../static/img/flutter_cli_task_expected_result.png)
