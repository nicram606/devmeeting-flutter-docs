---
id: create-first-flutter-app
title: Create your first Flutter app
---

Flutter maintainers made your beggining of the journey easy with Flutter CLI.

To create new Flutter app, use:
```shell
flutter create devmeetings_flutter_app
```

Output should look like this:
![console output](../../static/vid/flutter_create.gif)


