---
id: develop-with-ease
title: Hot reload in action
---

## Start the app
To start the app you can use GUI of your IDE (if proper Dart and Flutter plugins are installed) or just simply use the CLI in the `devmeetings_flutter_app` directory:
```shell
flutter run
```

### Test it out
Click the increment button a few times. Then delete the `:` from `You have pushed the button this many times:` sentence, save the changes, hit `r` button and watch how the magic of **stateful hot-reload happens**.

![stateful hot-reload demo](../../static/vid/flutter_hot_reload.gif)

Without losing counter's state widget was updated to display new text!

### Difference between hot-reload and hot-restart
There's also a mechanism called `hot-restart`, which restarts whole application without rebuiliding it. It's handy for changes that involves modification of eg. state logic.
