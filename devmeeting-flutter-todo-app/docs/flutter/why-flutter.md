---
id: why-flutter
title: Why Flutter?
---

Is Flutter really that great? Let's see:

## Typed language 
Flutter is built with Dart, Google's programming language. Dart is very similar to JavaScript, though typed and can be compiled. This means that:
- no special project configuration for TypeScript/Flow is required,
- IDEs can be more helpful with it's autocomplete capabilities,
- your app can be more performant after compilation to a binary.

## Fast growing community backed up by Google developer power
Flutter is open-source. Project is maintained by Google with code quality in mind but open for unrelated developers opinions.

:::note
Google conducts developer survey each quarter to ask about the direction Flutter should be headed.
:::

Non-Google Flutter community is growing fast, providing a lot of ready-made modules for your future app. Modules can be hosted on Google's platform, [pub.dev](https://pub.dev).

## Robust tooling
Flutter offers solutions for building, debugging and performance-profiling your app. Build troubleshooting is built in, as well as support for linting, module publishing, app minifing and many more. Google develops plugins for Intellij-based IDEs and Visual Studio Code to provide an easy way to use GUI all of the Flutter features.

## Great documentation
Documentation targeted for both beginners and migrating developers is available at [flutter.dev](https://flutter.dev/docs) with guides, samples and videos.

## One of the most loved frameworks of 2020
Flutter is in TOP 3 most loved framework of 2020 in StackOverflow Developer Survey, leaving behind alternatives such as **React Native** and **Xamarin**:

![TOP loved frameworks list from 2020 in StackOverflow Developer Survey](../../static/img/top3-loved-framework.png)

:::tip
Check full survey results [here](https://insights.stackoverflow.com/survey/2020).
:::
