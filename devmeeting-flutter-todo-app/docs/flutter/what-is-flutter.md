---
id: what-is-flutter
title: What is Flutter?
---

![Flutter logo](../../static/img/google-flutter-logo.webp)
_Source: https://venturebeat.com/wp-content/uploads/2018/02/google-flutter-logo.png_

Flutter is Google’s UI toolkit for building beautiful, natively compiled applications for mobile, web, and desktop from a single codebase.

## What Flutter can do?
Flutter delivers robust toolchain for creating mobile, web and soon desktop apps with native performance. Flutter allows developers to quickly create and adapt app's codebase with stateful hot reload while keeping it maintainable thanks to it's expressive and flexible UI.

## Who created Flutter?
Flutter is created and maintained as OSS by Google since **May 2017**.
:::note
First stable version was released at 4th December 2018.
:::

## Declarative framework
Flutter is _declarative_, meaning that it build UI to reflect the current state of your app.

![UI = f(state)](../../static/img/ui-equals-function-of-state.png)
_Source: https://flutter.dev/docs/development/data-and-backend/state-mgmt/declarative_

When the state of your app changes you can change the state. Changing the state will cause a redraw of the UI to reflect new data as **widgets**.

## Resemblence to React
Flutter's declarative syntax is inspired from the React's usage of JSX.
