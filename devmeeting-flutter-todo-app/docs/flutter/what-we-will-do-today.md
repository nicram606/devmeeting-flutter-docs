---
id: what-we-will-do-today
title: Goal for today
---

## Agenda

- Dart intro
  - Syntax
  - JS/TS comparison
- Flutter CLI
  - Run/build your app
  - Declarative UI syntax
  - Hot-reload
- Built-in widgets
  - Infinite scrollable list
  - Gesture recognition
- State management
  - `setState(() {})`
  - Use state in UI
  - Changing state based on user actions
- Make all widgets stateless
  - `pubspec.yaml` with `pub get`
  - Extract the state out of a widget
  - Streamable data sources
- Debugger
  - Run your app in debug mode
  - Use breakpoints to examine app's state

## App to build
![expected result](../../static/vid/flutter_stateful_list.gif)
