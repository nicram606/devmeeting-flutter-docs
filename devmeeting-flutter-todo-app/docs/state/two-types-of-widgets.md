---
id: two-types-of-widgets
title: Two types of widgets
---

## Stateless widgets
Stateless widgets are meant to be independent of any state. Those are just templates for the data that is passed in as arguments.

## Stateful widgets
Besides managing state with `setState()`, `State` classes have ability to detect when they are firstly built (`initState()`) and when they are destroyed (`dispose()`). Also, each update can be tracked with `didChangeDependencies()`.

![stateful widget lifecycle](../../static/img/flutter-stateful-widget-life-cycle.png)

Source: https://www.developerlibs.com/2019/12/flutter-lifecycle-widgets.html
