---
id: task
title: Task
---

- Create `TodoItem` list field in `_MyHomePageState`
- Add a `TextField` to `bottomSheet:` slot of `Scaffold` and handle `onSubmitted` event to create new TodoItem in list
- Wrap your `Todo` widget with `Dismissible` widget and handle left swipe gesture as done and right swipe gesture as delete Todo. Wire the Dismissible gestures to state updates
- 🌟 Create a background for Disimissible `Todo`s to give user gesture feedback
- Render the done Todos as crossed out and deleted as greyed out
- 🌟 Sort the todos by the state 

## Expected result
![stateful todo list demo](../../static/vid/flutter_stateful_list.gif)
