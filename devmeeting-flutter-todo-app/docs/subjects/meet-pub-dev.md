---
id: meet-pub-dev
title: Meet the pub.dev
---

## What is pub.dev
[pub.dev](https://pub.dev) is Dart's package manager website which allows you to explore available ready-made packages for your app. 

![pub.dev website](../../static/img/pub_dev_main.png)

## How to use
In root directory of your Flutter project there's a `pubspec.yaml`. It's a file that contains metadata about your project, including dependencies list.

Each package at [pub.dev](https://pub.dev) describes it's installation project in a `Installing` tab.

![example pub.dev package details](../../static/img/pub_dev_installing.png)
