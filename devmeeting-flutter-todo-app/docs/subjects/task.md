---
id: task
title: Task
---

- Add [rxdart](https://pub.dev/packages/rxdart) dependency to your Flutter project
- Create a TodoListSubject class for keeping and managing state updates. Replace the current state management entangled with widget's code with subject solution
- Refactor the StatefulWidget to Stateless one using the TodoListSubject class
