---
id: js-ts-comers-gotchas
title: Gotchas for JS/TS developers
---

Dart's syntax does have a few false friends for the JS/TS developers. Here are some of them to avoid confusion:

## Semicolons
Semicolons are **required** in Dart. It may be a subject to change in the future however, here's the [discussion](https://github.com/dart-lang/language/issues/69).

## const != const
Equivalent to JavaScript's `const` is a `final`.

`const` in Dart represents a **compile-time declared** value, not an unchangeable one.

## Comparison operator `===`
There are no `===` operator in Dart, `==` is the equivalent thanks to the Dart's type system.

## Promise is Future
In Dart, an idea of a task that will be finished later and can be awaited for is called `Future`, unlike in JS/TS.

## `async` keyword positioning
`async` keyword enabling `await` syntax in the function body is placed **after** the function name:
```dart
example() async {...}
```

## Fat arrow functions
This code in Dart
```dart
example() => true;
```
means that the `example()` function will return `true` and can have **only one operation performed**. For multistep operations, use:
```dart
example() {
  .. steps ..
  return true;
}
```
It works for lambdas too, like so
```dart
onEventHappened = () { .. do stuff .. }
```

`this` in Dart is not as confusing as in JS so one way of defining lambdas is enough 😉

## Method return types
Unlike in TS, return types are defined **before** function declaration:
```
bool example() {
  return true;
}
```

## Modules handling
No `export` keyword or `module.exports` object is present in Dart. Everything that is in the root level of the `.dart` file can be imported in other modules. To hide elements from other modules predicate the name of the element with `_`, effectively making it private for the module.

```dart
const importableByOtherModules = 100;
const _hiddenFromOtherModules = 101;
```

## String templates
In Dart, you can use templates in both `''` and `""` strings like so 
```dart
print('${method()}, $variable')
```
No backticks are required (or even supported).

