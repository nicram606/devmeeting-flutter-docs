---
id: task
title: Task
---

- Create a class for storing todos with `copyWith()` pattern applied. Class should be named TodoItem and consist of fields `title` of type String and `state` of type enum(created, finished, deleted). [help](./dart-basics)
- Override toString() method of TodoItem to display task details instead of default `Instance of 'TodoItem'`. Make use of string templates. [help](https://coflutter.com/dart-how-to-print-an-object/)
- Make a List of 3 TodoItems and print them to the terminal using forEach on the list in the `main` method. 

Use [dartpad](https://www.dartpad.dev/) to test your solution in the browser. 


:::warning
We will be using this code later. Save your code after you done the task somewhere safe for future reference. DartPad does save your latest creation in local cache, but it can be easly overwritten.
:::
