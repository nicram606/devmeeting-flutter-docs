---
id: meet-dart
title: Meet Dart, the JS killer
---

## Brief history of Dart
Unveiled at 10th October, 2011, Google's programming language was initially meant as a replacement for JavaScript. Later, in 2015, those plans were changed to allow developers to compile Dart source code to JavaScript source code via neatly named utility `dart2js`.

Because of Dart's native performance achieved by eg. AOT compilation there are many libraries created to introduce Dart as a backend solution.
:::note
Dart's backend ecosystem is surprisingly capable and can be viable option as an [Node.js](https://nodejs.org) with TypeScript or [Deno](https://deno.land/) alternative.

Eg. there's a HTTP library called [angel](https://angel-dart.dev/) and MongoDB driver [mongo_dart](https://pub.dev/packages/mongo_dart).
:::

## Who uses Dart
There're a lot of great apps built with Dart and Flutter. There's even a [list](https://flutter.dev/showcase), that includes [Tencent](https://www.tencent.com/en-us/index.html), [The New York Times](https://www.nytimes.com/) and more.

## Why Dart
Dart is a fully object-oriented and optionally typed language that can be compiled to achieve native performance.

Dart tooling supports JIT (Just In Time) and AOT (Ahead Of Time) compilation, which means it great for both development with seamless runtime changes insertion and for portability to the end user machine.

:::note
JIT compilation is used in hot-reload feature of Flutter, which speeds up the development a lot. We'll see about that later in the workshop.
:::
