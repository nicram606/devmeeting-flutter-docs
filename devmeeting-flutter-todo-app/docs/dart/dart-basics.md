---
id: dart-basics
title: Language Basics
---

Before creating our first Flutter app, let's see how Dart syntax works.

:::tip
We'll only scratch the surface of the Dart's syntax here that's necessary to complete today's tasks. If you want to learn more, head to official [language tour](https://dart.dev/guides/language/language-tour).
:::



## Variable definition
```dart
var hello = 'Hello, World!';
```
Variables store references. The variable called `hello` contains a reference to a `String` object with a value of “Hello, World!”.

:::note
The type of the name variable is inferred to be String, but you can change that type by specifying it.
:::

To specify that hello should be a `String` instance, use:
```dart
String hello = 'Hello, World!';
```

You can also allow to store more than one type of objects in `hello` variable by declaring it as a `dynamic`:
```dart
dynamic hello = 'Hello, World!';
hello = 1; // no compilation error!
```

### Final and const
If you never intend to change a variable, use final or const, either instead of var or in addition to a type. A final variable can be set only once; a const variable is a compile-time constant. 
:::note
Const variables are implicitly final.
:::
A final top-level or class variable is initialized the first time it’s used.

```dart
final name = 'Bob'; // Without a type annotation
final String nickname = 'Bobby';
```

Use const for variables that you want to be compile-time constants. Where you declare the variable, set the value to a compile-time constant such as a number or string literal, a const variable, or the result of an arithmetic operation on constant numbers:

```dart
const bar = 1000000; // Unit of pressure (dynes/cm2)
const double atm = 1.01325 * bar; // Standard atmosphere
const baz = []; // Equivalent to `const []`
```

## Classes
Class syntax:
```dart
class Point {
  double x;
  double y;

  Point(double x, double y) {
    this.x = x;
    this.y = y;
  }

  double distanceTo(Point other) {
    var dx = x - other.x;
    var dy = y - other.y;
    return sqrt(dx * dx + dy * dy);
  }
}
```

## Enums
Enum example: 
```dart
enum Color { red, green, blue }
```

## `copyWith()` pattern
There's a common pattern visible in a lot of Flutter data structures called `copyWith()`. Implementation looks like this:

```dart
class Point {
  double x;
  double y;

  Point({this.x, this.y});

  Point copyWith({double x, double y}) => 
      Point(x: x ?? this.x, y: y ?? this.y);
}
```

`copyWith()` pattern uses named parameters and conditional operator `??`. It allows easy modification of an instance values without loosing previous ones. It really handy when you want to edit just one of many fields of an object.

Example usage:
```dart
final point = Point(x: 10, 10);
final pointToTheRight = point.copyWith(x: 20);
```

:::tip
The `copyWith()` pattern is used in a lot of Flutter components, eg. [TextStyle](https://api.flutter.dev/flutter/painting/TextStyle-class.html). Let's imagine that you want to change just the text color, leaving font family, size, line height and other properties unchanged. `copyWith()` pattern allows you to do as a oneliner
```dart 
  final newTextStyle = textStyle.copyWith(color: newColor);
```
instead of redefining the whole object, like so
```dart
  final newTextStyle = TextStyle(
    color: newColor,
    fontFamily: textStyle.fontFamily,
    size: textStyle.size,
    height: textStyle.height,
    ...
  );
```
:::
