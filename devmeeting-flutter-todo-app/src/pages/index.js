import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const mentors = [
  {
    title: <>Marcin Wróblewski</>,
    imageUrl: 'img/m_wroblewski_avatar.jpg',
    description: (
      <>
        Fullstack developer | Flutter enthusiast | Today's speaker<br/><a href="https://fidev.io">marcin.wroblewscy.eu</a>
      </>
    ),
  },
  {
    title: <>Marcin Szałek</>,
    imageUrl: 'img/m_szalek_avatar.jpg',
    description: (
      <>
        Flutter enthusiast | Developer | Blogger at <a href="https://fidev.io">fidev.io</a> <br/>Speaker | Flutter Europe Organizer | Flutter Lodz Organizer
      </>
    ),
  },
];

const organizers = [
  {
    title: <>Inga Luczaj</>,
    imageUrl: 'https://devmeetings.org/reactHooks/img/inga_luczaj.jpeg',
    description: (
      <>
        <a href="https://devmeetings.org/">DevMeetings.org</a> Foundation CEO
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--6', styles.feature)}>
      {imgUrl && (
        <img className={styles.featureImage} src={imgUrl} alt={title} />
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Flutter intro workshops">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/')}>
              Begin
            </Link>
          </div>
        </div>
      </header>
      <main>
        <h1 className={styles.mentorsHeader}>Meet the team</h1>
        {mentors && mentors.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {mentors.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
        <h1 className={styles.mentorsHeader}>Made possible by</h1>
        {organizers && organizers.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {organizers.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;
