module.exports = {
  sidebar: {
    'Flutter Overview': [
      'flutter/what-is-flutter', 
      'flutter/why-flutter', 
      'flutter/what-we-will-do-today',
    ],
    'Dart language': [
      'dart/meet-dart', 
      'dart/dart-basics', 
      'dart/js-ts-comers-gotchas', 
      'dart/task',
    ],
    'Flutter CLI': [
      'flutter-cli/create-first-flutter-app', 
      'flutter-cli/how-does-flutter-look-like', 
      'flutter-cli/develop-with-ease', 
      'flutter-cli/task',
    ],
    'Widgets': [
      'widgets/what-is-a-widget', 
      'widgets/flutter-provided-widgets',
      'widgets/task',
    ],
    'State management': [
      'state/how-does-flutter-treat-state', 
      'state/two-types-of-widgets',
      'state/task',
    ],
    'Subjects': [ 
      'subjects/what-are-streams',
      'subjects/meet-pub-dev',
      'subjects/task',
    ],
  },
};
