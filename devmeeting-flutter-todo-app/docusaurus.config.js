module.exports = {
  title: 'Flutter Intro Workshop',
  tagline: 'Start your journey with Flutter here',
  url: 'https://flutter-intro-workshop.web.app',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  favicon: 'img/favicon.ico',
  organizationName: 'Marcin Wróblewski', // Usually your GitHub org/user name.
  projectName: 'devmeeting-flutter-docs', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Flutter Intro Workshop',
      logo: {
        alt: 'Flutter logo',
        src: 'img/logo.svg',
      },
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'DevMettings',
          items: [
            {
              label: 'Website',
              to: 'https://devmeetings.org/',
            },
            {
              label: 'Facebook group',
              to: 'https://www.facebook.com/groups/840776942776111/',
            },
            {
              label: 'Facebook',
              to: 'https://www.facebook.com/devmeetingspl/',
            },
            {
              label: 'Twitter',
              to: 'https://twitter.com/DevMeetingsPL',
            },
            {
              label: 'LinkedIn',
              to: 'https://www.linkedin.com/company/devmeetings.org/',
            },
          ],
        },
        {
          title: 'Fidev',
          items: [
            {
              label: 'Website',
              to: 'https://fidev.io/',
            },
            {
              label: 'YouTube',
              to: 'https://www.youtube.com/channel/UCM8qsVZh9pLKL-7uD8nmb0A',
            },
            {
              label: 'Twitter',
              to: 'https://twitter.com/marcin_szalek',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Marcin Wróblewski marcin.wroblewscy.eu<br>Flutter and the related logo are trademarks of Google LLC. This workshop is not affiliated with or otherwise sponsored by Google LLC.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          homePageId: 'flutter/what-is-flutter',
          sidebarPath: require.resolve('./sidebars.js'),
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
